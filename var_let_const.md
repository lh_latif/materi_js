# var, let, dan const
## Kegunaan masing-masing syntax
Sebenarnya ketiga syntax tersebut (_var_, _let_, _const_) digunakan untuk membuat variable. namun ada perbedaan besar antara syntax _var_ & _let_, dengan syntax _const_.

### `var` & `let`
digunakan untuk membuat variable, yang variable tersebut dapat diisi(di-_assign_) ulang.
```js
// value awal `a` adalah 1
var a = 1;
// di-assign ulang dengan value "ini value a"
a = "ini value a"
// tidak terjadi error

// begitupun untuk let

// value awal `b` adalah 2
let b = 2;
// di-assign ulang dengan value "ini value b"
b = "ini value b"
// tidak terjadi error
```

### `const`
_const_ yang merupakan kependekan dari _constant_. merupakan syntax untuk membuat variable yang valuenya menetap, atau tidak dapat diassign ulang.

```js
// value awal x adalah 1
const x = 1;
// di-assign ulang maka terjadi error
x = "ini x"
```
Meskipun `const` membuat sebuah variable tidak dapat diassign ulang. Namun jika value dari sebuah variable constant adalah object, masih dapat memodifikasi data/value didalam object tersebut.

```js
// value awal constant1 adalah object
const constant1 = {
  name: "constant",
  number: 1,
  newName: function(newName) {
    this.newName = newName;
  }
}
// meng-assign ulang value dari property number
constant1.number = 1000;
// menjalankan method pada object
constant1.newName("ini nama baru");

// tidak terjadi error

// ini juga berlaku untuk array
const array1 = [1,2,3];
array1.push(10);
array1.unshift()
array1.pop();
// tidak terjadi error
```
