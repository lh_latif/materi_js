# Command-command dasar pada Terminal Linux (Ubuntu)

Terminal merupakan tools utama programmer yang menggunakan OS Linux. Berikut command-command yang dapat digunakan untuk melakukan tugas-tugas dasar dalam manipulasi file dan folder.

- `ls` - kependekan dari `list`, fungsinya menampilkan list file dan folder dari suatu directory/path yang diberikan di paramter atau _current directory_.

  contoh:
  `ls ./`


- `cd` - kependekan dari `change directory`, pindah kedalam suatu path yang diberikan dalam parameter. Dan mengganti `current directory` dalam terminal.

  contoh:
  `cd ./project/my_project`

- `mkdir` - kependekan dari `make directory`, digunakan untuk membuat folder.

  contoh:
  `mkdir ./my_new_folder`

- `mv` - kependekan dari `move`, digunakan untuk memindahkan file atau folder. Juga bisa digunakan untuk mengganti nama file atau folder.

  contoh:
  `mv ./my_coding.erl ./main.erl`

- `cp` - kependedkan dari `cp`, digunakan untuk menggandakan file atau folder.

  contoh:
  `cp ./my_coding1.rs ./my_coding2.rs`

- `rm` - kependekan dari `remove`, digunakan untuk menghapus file atau folder.

  contoh:<br>
  `rm -R ./folder_lama` - Untuk folder<br>
  `rm ./codingan_lama.js` - Untuk file
