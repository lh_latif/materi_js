# Perkenalan Bagian Terminal Linux

![Terminal Screenshot](./demo0.png)

perhatikan tulisan dalam kotak hitam, yaitu teks:
`latif@aspire:~$`.

teks tersebut sebenarnya tersusun dalam urutan yang ditentukan. berikut penjelasan untuk setiap kata.

`latif` - merupakan nama __user__ (username)

`aspire` - merupakan nama __host__ (hostname)

`~` - menunjukkan directory yang aktif

`$` - merupakan `prompt symbol`. _prompt symbol_ dapat berupa karakter `$` atau `#`.

![Terminal Screenshot 2](./demo0-1.png)

gambar diatas adalah contoh terminal penggunaan terminal pada umumnya.
Misal ingin menjalankan program dapat di tulis seperti format pada teks digambar, yaitu pada teks:
`flutter create ./path/new_project`
.
- `flutter` - nama dari program yang akan dijalankan
- `create ./path/new_project` - adalah parameter / argument untuk command yang ingin dijalankan, dalam konteks ini adalah `flutter`. `create` merupakan parameter pertama, sedangkan `./path/new_project` merupakan parameter kedua.
