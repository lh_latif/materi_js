# Asynchronous Programming

## Asynchronous Programming?
Merupakan konsep pada pemrograman yang eksekusi sebuah procedure dapat ditunda/tidak dapat dieksekusi secara langsung. Lawan dari asynchronous programming adalah synchronous programming yang selalu mengeksekusi precedure secara langsung dan tidak dapat ditunda.

## Asynchronous Programming di JavaScript
Untuk memenuhi konsep asynchronous programming, di JavaScript dibuatkan sebuah class bawaan yaitu class Promise serta 2 syntax baru yaitu `async` dan `await`.

## class Promise
cara menggunakan class Promise, mirip seperti membuat object baru menggunakan class.
```js

// parameter untuk class Promise adalah high order function
var promise_obj = new Promise(
  // resolve dan reject merupakan high order function
  (resolve, reject) => {
    if (true) {
      // function pada resolve digunakan
      // untuk menyelesaikan Promise
      resolve();
    } else {
      // function pada reject digunakan
      // untuk menyatakan error / melanggar Promise
      reject();
    }
  }
)
```

## Menggunakan Object Promise
```js
var action = new Promise(
  (resolve, reject) => {
    resolve("nice")
  }
);
action.then((result) => {

})
```
