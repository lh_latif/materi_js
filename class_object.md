# Class dan Object

## Apa itu `Object`
Sebuah data-type yang memiliki struktur key dan value.

```
// object
{
  `key`: `value`
}
```

value dalam object dapat berisi value yang diisi dengan value literal atau variable.

```js
var name = "orang";
var object = {
  name: name,
  age: 16,
  birthplace: "Indonesia"
}
```

## Struktur Key dalam Object
Pada umumanya _key_ dalam _data-structure_ object diberi nama _property_ dan _method_.
- __property__

  merupakan _key_ yang tidak memiliki value ber-_data-type_ _function_, jadi valuenya dapat ber-_data-type_ _string_, _integer_, _boolean_, _array_, _object_, _null_.

- __method__

  merupakan _key_ yang memiliki value ber-_data-type_ _function_.

## Konsep Class
Syntax `class` dapat digunakan untuk membuat sebuah class. Class sendiri digunakan sebagai cetakan untuk membuat object. misal class Post:
```js
// membuat class dengan cara
// menggunakan syntax class diikuti dengan Namanya
// nama dari class harus Kapital

class Post {  
  constructor(title, content) {
    // membuat property menggunakan this.<property>
    this.title = title;
    this.content = content;
  }


  updateTitle(newTitle) {
    this.title = newTitle
  }

  // method yang berkaitan dengan post
  printPost() {
    console.log(`${this.title} ${this.content}`)
  }
}
```

## Membuat object dengan class
Membuat object menggukan class yaitu dengan menggunakan syntax `new` lalu diikuti dengan memanggil class tersebut.

```js
// membuat object Post
new Post("ini title", "ini content")

// membuat object Post lalu disimpan di variable
var post_1 = new Post("Ini title Post 1", "Ini Content Post 1")
```
## Mendapatkan property pada object
```js
var post_2 = new Post("Ini title Post 2", "Ini Content Post 2")
// mengakses property pada object yaitu dengan
// menulis nama variable yang berisi object
// dikikut dengan dot (.) lalu nama property-nya
post_2.title; // mendapatkan value "Ini title Post 2"
```

## Memanggil method pada object
```js
// memanggil method pada object
post_2.printPost(); // memanggil method
```
