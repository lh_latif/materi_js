# Mengendalikan error pada JavaScript menggunakan try-catch
## Apa itu try-catch?
Merupakan syntax pengendali error yang terdiri dari 2 bagian. Bagian pertama yaitu `try` untuk mengeksekusi presedure yang kemungkinan akan melempar (throw) / terjadi error. Lalu bagian kedua yaitu `catch` dan lainnya, untuk mengendalikan error.

Penulisan dasar syntax try-catch:
```js
try {
  // bagian pertama
  // procedure yang dieksekusi
  // dan kemungkinan error
} catch(error) {
  // bagian kedua
  // untuk menangkap error
}
```
